﻿using Briq.Repository.ViewModels;
using Briq.Repository.Definitions;
using Briq.Service.Definitions;
using System;
using System.Collections.Generic;

namespace Briq.Service.Implementations
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository OrderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            OrderRepository = orderRepository;
        }

        public List<Product> GetAllProducts(string name, int? priceFrom, int? priceTo)
        {
            return OrderRepository.GetAllProducts(name, priceFrom, priceTo);
        }

        public Order GetOrderById(int id)
        {
            return OrderRepository.GetOrderById(id);
        }

        public List<ProductsSold> GetNumberSoldProducts(DateTime startDate, DateTime endDate)
        {
            return OrderRepository.GetNumberSoldProducts(startDate, endDate);
        }

        public int? CreateOrder(NewOrderRequest request)
        {
            return OrderRepository.CreateOrder(request);
        }

        public List<List<ProductCombination>> GetOrderCombinations(int price)
        {
            return OrderRepository.GetOrderCombinations(price);
        }
    }
}
