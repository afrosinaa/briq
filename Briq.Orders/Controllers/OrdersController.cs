﻿using System;
using System.Collections.Generic;
using Briq.Repository.ViewModels;
using Briq.Service.Definitions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Briq.Orders.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService OrderService;

        public OrdersController(IOrderService orderService)
        {
            OrderService = orderService;
        }

        // GET api/orders/products
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("products")]
        public ActionResult<IEnumerable<Product>> GetProducts(string name, int? priceFrom, int? priceTo)
        {
            IEnumerable<Product> response = OrderService.GetAllProducts(name, priceFrom, priceTo);
            return Ok(response);
        }

        // GET api/orders/products/sales
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("products/sales")]
        public ActionResult<IEnumerable<ProductsSold>> GetNumberSoldProducts(DateTime startDate, DateTime endDate)
        {
            if (startDate != null && endDate != null)
            {
                IEnumerable<ProductsSold> response = OrderService.GetNumberSoldProducts(startDate, endDate);
                return Ok(response);
            }
            return BadRequest("StartDate and EndDate are mandatory fields");
        }

        // GET api/orders/products/combinations
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("products/combinations")]
        public ActionResult<IEnumerable<IEnumerable<ProductCombination>>> GetOrderCombinations(int price)
        {
            if (price <=0 )
                return BadRequest("Price is incorrect");

                IEnumerable<IEnumerable<ProductCombination>> response = OrderService.GetOrderCombinations(price);
                return Ok(response);
        }

        // GET api/orders/5
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public ActionResult<Order> GetOrder(int id)
        {
            Order response = OrderService.GetOrderById(id);
            if (response == null)
                return NotFound("Order not found");

            return Ok(response);

        }

        // POST api/orders
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<string> Post([FromBody] NewOrderRequest request)
        {
            if(request.productId <= 0 || request.numberOfPeople == 0)
                return BadRequest("Product and quantity are incorrect");
            
            int? orderCreated = OrderService.CreateOrder(request);
            if (orderCreated == null)
                return NotFound("Product does not exist");

                return Ok("Success");
        }
    }
}
