﻿using Briq.Repository.Definitions;
using Briq.Repository.Implementations;
using Briq.Service.Definitions;
using Briq.Service.Implementations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Briq.Orders
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddScoped<IOrderService, OrderService>();

            services.AddScoped<IOrderRepository, OrderRepository>();
            
            //Register Swagger generator
            services.AddSwaggerGen(c => c.SwaggerDoc(name: "v1", new Swashbuckle.AspNetCore.Swagger.Info{ Title = "Briq API", Version = "v1" }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
            
            app.UseSwagger();

            app.UseSwaggerUI(c => c.SwaggerEndpoint(url: "v1/swagger.json", name: "Briq API v1"));

        }
    }
}
