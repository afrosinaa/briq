﻿
using Briq.Repository.Definitions;
using Briq.Repository.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Briq.Repository.Implementations
{
    public class OrderRepository : IOrderRepository
    {
        private readonly Db DbContext;

        public OrderRepository()
        {
            using (StreamReader r = new StreamReader("db.json"))
            {
                string json = r.ReadToEnd();
                Db items = JsonConvert.DeserializeObject<Db>(json);

                DbContext = items;
            }
        }

        public List<Order> AddOrder(int orderId)
        {
            Db test = DbContext;
            throw new NotImplementedException();
        }

        public List<Product> GetAllProducts(string name, int? priceFrom, int? priceTo)
        {
            List<Product> allProducts = DbContext.Products;

            if (!string.IsNullOrEmpty(name))
                allProducts = allProducts.FindAll(product => product.name.ToLower().Contains(name.ToLower()));
            if (priceFrom != null)
                allProducts = allProducts.FindAll(product => product.price >= priceFrom);
            if (priceTo != null)
                allProducts = allProducts.FindAll(product => product.price <= priceTo);

            if (allProducts == null)
                return new List<Product>();

            return allProducts;
        }

        public List<ProductsSold> GetNumberSoldProducts(DateTime startDate, DateTime endDate)
        {
            List<ProductsSold> soldProducts = new List<ProductsSold>();
            IEnumerable<int> orderIdsPerPeriod = GetOrderIdsByDate(startDate, endDate);
            DbContext.Products.ForEach(product =>
            {
                List<OrderLine> orderLinesPerProduct = GetOrderLinesByProductId(product.id);
                IEnumerable<int> productOrderIds = orderLinesPerProduct.GroupBy(orderLine => orderLine.order_id).Select(x => x.Key);

                soldProducts.Add(new ProductsSold
                {
                    id = product.id,
                    name = product.name,
                    price = product.price,
                    times_sold = productOrderIds.Intersect(orderIdsPerPeriod).Distinct().Count()
                });
            });
            return soldProducts;
        }

        public int? CreateOrder(NewOrderRequest request)
        {
            Product selectedProduct = DbContext.Products.Find(product => product.id == request.productId);
            if (selectedProduct == null)
                return null;

            Order newOrder = new Order
            {
                currency = string.IsNullOrEmpty(request.currency) ? "EUR" : request.currency,
                id = GetNewOrderId(),
                datetime_utc = DateTime.UtcNow,
                amount = request.numberOfPeople * selectedProduct.price,
                number_of_persons = request.numberOfPeople,
                status = "Active"
            };

            OrderLine newOrderLine = new OrderLine
            {
                id = GetNewOrderLineId(),
                price = selectedProduct.price,
                order_id = newOrder.id,
                product_id = request.productId,
                quantity = request.numberOfPeople
            };

            DbContext.Orders.Add(newOrder);
            DbContext.Order_lines.Add(newOrderLine);

            return newOrder.id;
        }

        public Order GetOrderById(int id)
        {
            Order filteredOrder = DbContext.Orders.Find(order => order.id == id);

            if (filteredOrder != null)
                filteredOrder.order_lines = GetOrderLinesByOrderId(id);
            return filteredOrder;
        }

        public List<OrderLine> GetOrderLinesByOrderId(int orderId)
        {
            return DbContext.Order_lines.FindAll(orderLine => orderLine.order_id == orderId);
        }

        private List<OrderLine> GetOrderLinesByProductId(int productId)
        {
            return DbContext.Order_lines.FindAll(orderLine => orderLine.product_id == productId);
        }

        private IEnumerable<int> GetOrderIdsByDate(DateTime startDate, DateTime endDate)
        {
            return DbContext.Orders
                .FindAll(order => order.datetime_utc >= startDate && order.datetime_utc <= endDate)
                .Select(order => order.id);
        }

        private int GetNewOrderId()
        {
            return DbContext.Orders.Max(order => order.id) + 1;
        }

        private int GetNewOrderLineId()
        {
            return DbContext.Order_lines.Max(orderLine => orderLine.id) + 1;
        }

        // not finished
        public List<List<ProductCombination>> GetOrderCombinations(int targetPrice)
        {
            List<Product> products = DbContext.Products;

            List<List<ProductCombination>> result = new List<List<ProductCombination>>();

            return result;
        }
    }
}
