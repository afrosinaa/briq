﻿using System.Collections.Generic;

namespace Briq.Repository.ViewModels
{
    public class Db
    {
        public List<Product> Products { get; set; }
        public List<Order> Orders { get; set; }
        public List<OrderLine> Order_lines { get; set; }
    }
}
