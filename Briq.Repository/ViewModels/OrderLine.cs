﻿using System.ComponentModel.DataAnnotations;

namespace Briq.Repository.ViewModels
{
    public class OrderLine
    {
        public int id { get; set; }
        [Required]
        public int product_id { get; set; }
        public int order_id { get; set; }
        public int quantity { get; set; }
        public double price { get; set; }
    }
}
