﻿
namespace Briq.Repository.ViewModels
{
    public class Product
    {
        public int id { get; set; }
        public string name { get; set; }
        public double price { get; set; }
    }
}
