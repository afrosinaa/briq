﻿namespace Briq.Repository.ViewModels
{
    public class NewOrderRequest
    {
        public int productId { get; set; }
        public int numberOfPeople { get; set; }
        public string currency { get; set; }
    }
}
