﻿using System;
using System.Collections.Generic;

namespace Briq.Repository.ViewModels
{
    public class Order
    {
        public int id { get; set; }
        public DateTime datetime_utc { get; set; }
        public double amount { get; set; }
        public string currency { get; set; }
        public int number_of_persons { get; set; }
        public string status { get; set; }
        public List<OrderLine> order_lines { get; set; }
    }
}
