﻿using Briq.Repository.ViewModels;
using System;
using System.Collections.Generic;

namespace Briq.Repository.Definitions
{
    public interface IOrderRepository
    {
        List<Product> GetAllProducts(string name, int? priceFrom, int? priceTo);
        Order GetOrderById(int id);
        List<Order> AddOrder(int orderId);
        List<ProductsSold> GetNumberSoldProducts(DateTime startDate, DateTime endDate);
        int? CreateOrder(NewOrderRequest request);
        List<List<ProductCombination>> GetOrderCombinations(int price);
    }
}
